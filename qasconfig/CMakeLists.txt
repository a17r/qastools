# Program name

IF ( NOT PROGRAM_TITLE )
	SET ( PROGRAM_TITLE "QasConfig" )
ENDIF ( NOT PROGRAM_TITLE )

IF ( NOT PROGRAM_NAME )
	SET ( PROGRAM_NAME "qasconfig${PROGRAM_SUFFIX}" )
ENDIF ( NOT PROGRAM_NAME )

string ( TOUPPER ${PROGRAM_NAME} PROGRAM_NAME_UCASE )


# Program version

SET ( VERSION_MAJOR ${PACKAGE_VERSION_MAJOR} )
SET ( VERSION_MINOR ${PACKAGE_VERSION_MINOR} )
SET ( VERSION_PATCH ${PACKAGE_VERSION_PATCH} )

SET ( VERSION "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}${VERSION_SUFFIX}" )


# Process subdirectories

ADD_SUBDIRECTORY ( src )
ADD_SUBDIRECTORY ( graphics )
ADD_SUBDIRECTORY ( share )
